﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using QS.Models;

namespace QS.Controllers
{
    public class BookingsController : Controller
    {
        private QSDbContext db = new QSDbContext();

        // GET: Bookings
        public ActionResult Index()
        {
            return View(db.Bookings.ToList());
        }

        // GET: Bookings/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Booking booking = db.Bookings.Find(id);
            if (booking == null)
            {
                return HttpNotFound();
            }
            return View(booking);
        }

        // GET: Bookings/Create
        public ActionResult Create()
        {
            Booking booking = new Booking();
            Random random = new Random();
            booking.BookingIdentifire ="QS"+random.Next(1000000).ToString();
            booking.BookingStatus = "Panding";
            var SessionVaribale = Session["UserName"];
            if (SessionVaribale != null)
            {
                if (Session["UserName"].ToString() != "")
                {
                    booking.BookingUser = Session["UserName"].ToString();
                }
            }
            else
            {
                return RedirectToAction("Login", "User");
            }
           
            return View(booking);
        }

        // POST: Bookings/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "BookingId,BookingIdentifire,BookingPickupDate,BookingPickupTime,BookingParsalType,DelevaryName,DelevaryCountry,DelevaryCity,DelevaryAddress,BookingParsalWeight,BookingStatus,BookingUser")] Booking booking)
        {
            if (ModelState.IsValid)
            {
                db.Bookings.Add(booking);
                db.SaveChanges();
                BookingState state = new BookingState();
                state.BookingDateTime = DateTime.Now;     
                state.BookingIdentifire = booking.BookingIdentifire;
                state.State = booking.BookingStatus;
                db.BookingStates.Add(state);
                db.SaveChanges();
                ViewData["Booking"] = "Booking create Sucess";
            }

            return View(booking);
        }

        // GET: Bookings/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Booking booking = db.Bookings.Find(id);
            booking.BookingStatus = "Panding";
            if (booking == null)
            {
                return HttpNotFound();
            }
            return View(booking);
        }

       
        // POST: Bookings/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "BookingId,BookingIdentifire,BookingPickupDate,BookingPickupTime,BookingParsalType,DelevaryName,DelevaryCountry,DelevaryCity,DelevaryAddress,BookingParsalWeight,BookingStatus")] Booking booking)
        {
            if (ModelState.IsValid)
            {
                db.Entry(booking).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(booking);
        }

        // GET: Bookings/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Booking booking = db.Bookings.Find(id);
            if (booking == null)
            {
                return HttpNotFound();
            }
            return View(booking);
        }

        // POST: Bookings/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Booking booking = db.Bookings.Find(id);
            db.Bookings.Remove(booking);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
