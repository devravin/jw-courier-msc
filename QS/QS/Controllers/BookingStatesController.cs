﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using QS.Models;

namespace QS.Controllers
{
    public class BookingStatesController : Controller
    {
        private QSDbContext db = new QSDbContext();
        [HttpGet]
        public ActionResult TracState()
        {
            return View(db.BookingStates.ToList());
        }
        [HttpPost]
        public ActionResult TracState(string SearchIdentifire)
        {
            return View(db.BookingStates.ToList().Where(x=>x.BookingIdentifire==SearchIdentifire).ToList());
        }
        // GET: BookingStates
        public ActionResult Index()
        {
            return View(db.BookingStates.ToList());
        }

        // GET: BookingStates/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BookingState bookingState = db.BookingStates.Find(id);
            if (bookingState == null)
            {
                return HttpNotFound();
            }
            return View(bookingState);
        }

        // GET: BookingStates/Create
        public ActionResult Create()
        {
            BookingState bookingState = new BookingState();

            bookingState.BookingDateTime = DateTime.Now;

            return View(bookingState);
        }

        // POST: BookingStates/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "BookingStateId,BookingIdentifire,BookingDateTime,State")] BookingState bookingState)
        {
            if (ModelState.IsValid)
            {
                db.BookingStates.Add(bookingState);
                db.SaveChanges();
                Booking booking =  db.Bookings.Where(x => x.BookingIdentifire == bookingState.BookingIdentifire).FirstOrDefault();
                booking.BookingStatus = bookingState.State;
                db.Entry(booking).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", "Bookings");
               
             
            }

            return View(bookingState);
        }

        // GET: BookingStates/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BookingState bookingState = db.BookingStates.Find(id);
            if (bookingState == null)
            {
                return HttpNotFound();
            }
            return View(bookingState);
        }

        // POST: BookingStates/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "BookingStateId,BookingIdentifire,BookingDateTime,State")] BookingState bookingState)
        {
            if (ModelState.IsValid)
            {
                db.Entry(bookingState).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(bookingState);
        }

        // GET: BookingStates/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BookingState bookingState = db.BookingStates.Find(id);
            if (bookingState == null)
            {
                return HttpNotFound();
            }
            return View(bookingState);
        }

        // POST: BookingStates/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            BookingState bookingState = db.BookingStates.Find(id);
            db.BookingStates.Remove(bookingState);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
