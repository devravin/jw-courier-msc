﻿using QS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace QS.Controllers
{
    public class UserController : Controller
    {
        public ActionResult QuotePrice()
        {
            return View();
        }
        [HttpGet]
        public ActionResult TracState()
        {
            //return View(db.BookingStates.ToList());
            return View(new List<BookingState>());
        }

        [HttpPost]
        public ActionResult TracState(string SearchIdentifire)
        {
            return View(db.BookingStates.ToList().Where(x => x.BookingIdentifire == SearchIdentifire).ToList());
        }

        [HttpGet]
        public ActionResult MyTrackingStates()
        {
            var user = Session["User"] as QS.Models.User;
            if (user == null)
                return RedirectToAction("Login", "User");

            var bookings = db.Bookings.ToList().Where(x => x.BookingUser == user.UserName).ToList();
            //var bookingStates = new List<BookingState>();
            //foreach (var b in bookings)
            //{
            //    bookingStates.Add(new BookingState { BookingStateId = b.BookingId, BookingIdentifire = b.BookingIdentifire, BookingDateTime = b.BookingPickupTime, State = b.BookingStatus });
            //}

            return View(bookings);
        }

        [HttpGet]
        public ActionResult Home()
        {
            return View();
        }
        QSDbContext db = new QSDbContext();
        // GET: User
        [HttpGet]
        public ActionResult SignUp()
        {
            return View();
        }

        // GET: User
        [HttpPost]
        public ActionResult SignUp(User user)
        {
            if (ModelState.IsValid)
            {
                user.UserType = 1;
                db.Users.Add(user);
                db.SaveChanges();
                return RedirectToAction("Login");
            }
            return View();
        }

        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        // GET: User
        [HttpPost]
        public ActionResult Login(Login user)
        {
            if (ModelState.IsValid)
            {
                if(db.Users.Where(x=>x.UserName==user.UserName && x.Password == user.Password).Any())
                {
                    var curruntUser = db.Users.SingleOrDefault(u => u.UserName == user.UserName && u.Password == user.Password);

                    if (db.Users.Where(x => x.UserName == user.UserName && x.Password == user.Password).Select(x=>x.UserType).FirstOrDefault() == 1)
                    {
                        TempData["User"] = user.UserName;
                        Session["UserName"] = user.UserName;
                        Session["User"] = curruntUser;
                        Session["userType"] = curruntUser.UserType;

                        return RedirectToAction("Index","Home");
                    }
                    else if((db.Users.Where(x => x.UserName == user.UserName && x.Password == user.Password).Select(x => x.UserType).FirstOrDefault() == 2))
                    {
                        TempData["User"] = user.UserName;
                        Session["UserName"] = user.UserName;
                        Session["User"] = curruntUser;
                        Session["userType"] = curruntUser.UserType;
                        return RedirectToAction("Home", "Admin");
                    }

                }
                else
                {
                    ViewData["error"] = "user name or password is wrong";
                }
                
            }
            return View();
        }
    }
}