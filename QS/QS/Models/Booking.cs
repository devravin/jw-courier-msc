﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace QS.Models
{
    public class Booking
    {
        public int BookingId { get; set; }
        [Required]
        [Display(Name = "Tracking Number")]
        public string BookingIdentifire { get; set; }
        [Required]
        [Display(Name = "Pickup Date")]
        [DataType(DataType.Date)]
        public DateTime BookingPickupDate { get; set; }
        [Required]
        [Display(Name = "Pickup Time")]
        [DataType(DataType.Time)]
        public DateTime BookingPickupTime { get; set; }
        [Required]
        [Display(Name = "Parcel Type")]
        public string BookingParsalType { get; set; }
        [Required]
        [Display(Name = "Delivery Name")]
        public string DelevaryName { get; set; }
        [Required]
        [Display(Name = "Delivery Country")]
        public string DelevaryCountry { get; set; }
        [Required]
        [Display(Name = "Delivery City")]
        public string DelevaryCity { get; set; }
        [Required]
        [Display(Name = "Delivery Address")]
        public string DelevaryAddress { get; set; }
        [Required]
        [Display(Name = "Parcel Wieght")]
        public string BookingParsalWeight { get; set; }

        [Required]
        [Display(Name = "Booking Status")]
        public string BookingStatus { get; set; }

        //   [Required]
        //   public string BookingPrice { get; set; }

        [Display(Name = "Booking User")]
        public string BookingUser { get; set; }
    }
}