﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace QS.Models
{
    public class BookingState
    {
        public int BookingStateId { get; set; }

        [Required]
        public string BookingIdentifire { get; set; }

        public DateTime BookingDateTime { get; set; }

        public string State { get; set; }
    }
}